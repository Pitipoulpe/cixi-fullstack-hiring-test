import update                                   from 'immutability-helper';
import {
    OPEN_MODAL,
    CLOSE_MODAL,
}                                               from './modalsActions';
import { asyncActionError, asyncActionSuccess } from '../helpers/asyncAction';

const initialState = {
    currentModal: null
};

const modalsReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case OPEN_MODAL: {
            return update(state, {
                currentModal: {$set: action.modal}
            });
        }
        case CLOSE_MODAL: {
            return update(state, {
                currentModal: {$set: null}
            });
        }
        default:
            return state;
    }
};

export default modalsReducer;
