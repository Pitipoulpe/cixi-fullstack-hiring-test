import { MODALS }                              from './modalsActions';
import { compose, find, pick, pipe, property } from 'lodash/fp';

const stateSelector = property(`${MODALS}`);

export const currentModalSelector = pipe(
    stateSelector,
    pick('currentModal')
);

