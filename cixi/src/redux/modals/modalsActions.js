export const MODALS = 'MODALS';
export const OPEN_MODAL = `${MODALS}/OPEN`;
export const CLOSE_MODAL = `${MODALS}/CLOSE`;


export const openModal = modal => dispatch => {
    dispatch({
        type: OPEN_MODAL,
        modal
    });

};

export const closeModal = cb =>  dispatch => {
    dispatch({type: CLOSE_MODAL});
    if (typeof cb === 'function') cb();
};
