import { ROADS }                              from './roadsActions';
import { pick, pipe, property } from 'lodash/fp';
import { CONTENT }                      from '../contentActions';

const stateSelector = property(`${CONTENT}.${ROADS}`);

export const roadsSelector = pipe(
    stateSelector,
    pick('roads')
);

