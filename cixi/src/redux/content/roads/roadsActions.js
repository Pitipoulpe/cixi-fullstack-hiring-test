import { asyncActionError, asyncActionSuccess }                   from '../../helpers/asyncAction';
import { fetchRoads as _fetchRoads, createRoad as _createRoad } from './roadsQuery';

export const ROADS = 'ROADS';
export const FETCH_ROADS = `${ROADS}/FETCH`;
export const CREATE_ROAD = `${ROADS}/CREATE`;


export const fetchRoads = cb => async (dispatch, _, client) => {
    dispatch({type: FETCH_ROADS});
    try {
        const request = await client.query({
            query: _fetchRoads
        });
        const roads = request.data?.getRoads;
        dispatch({
            type: asyncActionSuccess(FETCH_ROADS),
            roads
        });
        if (typeof cb === 'function') cb(null, roads);
    } catch (error) {
        dispatch({
            type: asyncActionError(FETCH_ROADS),
            error
        });
        if (typeof cb === 'function') cb(error);
    }
};

export const createRoad = (from, to, distance, cb) => async (dispatch, _, client) => {
    dispatch({type: CREATE_ROAD});
    try {
        const request = await client.mutate({
            mutation: _createRoad,
            variables: {
                from, to, distance: parseInt(distance)
            }
        });
        const road = request.data?.addRoad;

        dispatch({
            type: asyncActionSuccess(CREATE_ROAD),
            road
        });
        if (typeof cb === 'function') cb(null, road);
    } catch (error) {
        dispatch({
            type: asyncActionError(CREATE_ROAD),
            error
        });
        if (typeof cb === 'function') cb(error);
    }
};
