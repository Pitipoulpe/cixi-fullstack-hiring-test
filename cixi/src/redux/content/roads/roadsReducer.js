import update                                   from 'immutability-helper';
import {
    CREATE_ROAD,
    FETCH_ROADS,
}                                               from './roadsActions';
import { asyncActionError, asyncActionSuccess } from '../../helpers/asyncAction';

const initialState = {
    roads : [],
    isLoading: false,
    isLoaded: false,
    error: null
};

const roadsReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case FETCH_ROADS: {
            return update(state, {
                isLoading:  {$set: true},
                isLoaded: {$set: false}
            });
        }
        case asyncActionSuccess(FETCH_ROADS): {
            return update(state, {
                roads: {$set: action.roads},
                isLoading:  {$set: false},
                isLoaded: {$set: true}
            });
        }
        case asyncActionError(FETCH_ROADS): {
            return update(state, {
                isLoading:  {$set: false},
                isLoaded: {$set: false},
                error: {$set: action.error}
            });
        }
        case CREATE_ROAD: {
            return update(state, {
                isLoading:  {$set: true},
                isLoaded: {$set: false}
            });
        }
        case asyncActionSuccess(CREATE_ROAD): {
            return update(state, {
                roads: {$push: [action.road]},
                isLoading:  {$set: false},
                isLoaded: {$set: true}
            });
        }
        case asyncActionError(CREATE_ROAD): {
            return update(state, {
                isLoading:  {$set: false},
                isLoaded: {$set: false},
                error: {$set: action.error}
            });
        }
        default:
            return state;
    }
};

export default roadsReducer;
