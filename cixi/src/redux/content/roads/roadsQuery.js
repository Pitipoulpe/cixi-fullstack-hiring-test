import { gql } from 'apollo-boost';

export const fetchRoads = gql`
query {
    getRoads { id, from { name }, to { name }, distance }
}
`;

export const createRoad = gql`
mutation AddRoad($from:String!, $to:String!, $distance:Int!) {
    addRoad(from: $from, to: $to, distance: $distance) { from { name }, to { name }, distance }
}
`;

