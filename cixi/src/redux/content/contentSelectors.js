import { CONTENT } from './contentActions';
import { property } from 'lodash/fp';
export const stateSelector = property(CONTENT);
