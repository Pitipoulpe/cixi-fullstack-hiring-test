import { combineReducers } from 'redux';
import { CITIES }          from './cities/citiesActions';
import citiesReducer       from './cities/citiesReducer';
import { ROADS }           from './roads/roadsActions';
import roadsReducer        from './roads/roadsReducer';

export default combineReducers({
    [CITIES]: citiesReducer,
    [ROADS]: roadsReducer
});
