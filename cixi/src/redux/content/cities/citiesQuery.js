import { gql } from 'apollo-boost';

export const fetchCities = gql`
query {
    getCities {name}
}
`;

export const createCity = gql`
mutation AddCity($name:String!) {
    addCity(name: $name) {name}
}
`;
