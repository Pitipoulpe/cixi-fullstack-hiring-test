import update                 from 'immutability-helper';
import {
    CREATE_CITY,
    FETCH_CITIES,
} from './citiesActions';
import { asyncActionError, asyncActionSuccess } from '../../helpers/asyncAction';
import get_clamped_value from '../../../helpers/clamped_value'

const initialState = {
    cities : [],
    isLoading: false,
    isLoaded: false,
    error: null
};

const getXY = () => ({
    x:  get_clamped_value(Math.random() * window.innerWidth, 100, window.innerWidth - 100),
    y: get_clamped_value(Math.random() * 500, 50, 450)
});

const citiesReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case FETCH_CITIES: {
            return update(state, {
                isLoading:  {$set: true},
                isLoaded: {$set: false}
            });
        }
        case asyncActionSuccess(FETCH_CITIES): {
            return update(state, {
                cities: {$set: action.cities.map( el => ({
                        ...el,
                        ...getXY()
                    }))},
                isLoading:  {$set: false},
                isLoaded: {$set: true}
            });
        }
        case asyncActionError(FETCH_CITIES): {
            return update(state, {
                isLoading:  {$set: false},
                isLoaded: {$set: false},
                error: {$set: action.error}
            });
        }
        case CREATE_CITY: {
            return update(state, {
                isLoading:  {$set: true},
                isLoaded: {$set: false}
            });
        }
        case asyncActionSuccess(CREATE_CITY): {
            return update(state, {
                cities: {$push: [{
                        ...action.city,
                        ...getXY()
                    }]},
                isLoading:  {$set: false},
                isLoaded: {$set: true}
            });
        }
        case asyncActionError(CREATE_CITY): {
            return update(state, {
                isLoading:  {$set: false},
                isLoaded: {$set: false},
                error: {$set: action.error}
            });
        }
        default:
            return state;
    }
};

export default citiesReducer;
