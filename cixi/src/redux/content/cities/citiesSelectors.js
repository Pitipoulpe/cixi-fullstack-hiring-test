import { CITIES }                     from './citiesActions';
import { compose, find, pick, pipe, property }            from 'lodash/fp';
import { CONTENT }                      from '../contentActions';

const stateSelector = property(`${CONTENT}.${CITIES}`);

export const citiesSelector = pipe(
    stateSelector,
    pick('cities')
);

