import { asyncActionError, asyncActionSuccess } from '../../helpers/asyncAction';
import { fetchCities as _fetchCities, createCity as _createCity }          from './citiesQuery';

export const CITIES = 'CITIES';
export const FETCH_CITIES = `${CITIES}/FETCH`;
export const CREATE_CITY = `${CITIES}/CREATE`;


export const fetchCities = cb => async (dispatch, _, client) => {
    dispatch({type: FETCH_CITIES});
    try {
        const request = await client.query({
            query: _fetchCities
        });
        const cities = request.data?.getCities;
        dispatch({
            type: asyncActionSuccess(FETCH_CITIES),
            cities: cities
        });
        if (typeof cb === 'function') cb(null, cities);
    } catch (error) {
        dispatch({
            type: asyncActionError(FETCH_CITIES),
            error
        });
        if (typeof cb === 'function') cb(error);
    }
};

export const createCity = (name, cb) => async (dispatch, _, client) => {
    dispatch({type: CREATE_CITY});
    try {
        const request = await client.mutate({
            mutation: _createCity,
            variables: {
                name
            }
        });
        const city = request.data?.addCity;

        dispatch({
            type: asyncActionSuccess(CREATE_CITY),
            city
        });
        if (typeof cb === 'function') cb(null, city);
    } catch (error) {
        dispatch({
            type: asyncActionError(CREATE_CITY),
            error
        });
        if (typeof cb === 'function') cb(error);
    }
};
