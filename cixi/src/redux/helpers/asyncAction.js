export const asyncActionSuccess = name => `${name}/SUCCESS`;
export const asyncActionError = name => `${name}/ERROR`;
