import ApolloClient from 'apollo-boost';
// const isDev = process.env.NODE_ENV !== 'production';
const client = new ApolloClient({
    uri: 'http://localhost:4000/api',
    /*request: (operation) => {
        const token = localStorage.getItem('authToken');
        operation.setContext({
            headers: {
                authorization: token ? `Bearer ${token}` : ''
            }
        })
    }*/
});

export default client;
