import {
    applyMiddleware,
    createStore,
    combineReducers,
    compose
} from 'redux';
import thunk from 'redux-thunk';

import { CONTENT }    from './content/contentActions';
import contentReducer from './content/contentReducer';
import { MODALS }     from './modals/modalsActions';
import modalsReducer  from './modals/modalsReducer';
import client         from './apolloClient';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middlewares = [ thunk.withExtraArgument(client) ];

const store = createStore(
    combineReducers({
        [CONTENT]: contentReducer,
        [MODALS]: modalsReducer
    }),
    {
    },
    composeEnhancers(
        applyMiddleware( ...middlewares )
    )
);

export default store;
