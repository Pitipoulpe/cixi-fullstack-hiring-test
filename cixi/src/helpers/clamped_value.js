const get_clamped_value = ( value, min, max ) => {

    if ( value > max ) value = max;
    if ( value < min ) value = min;

    return value;
};

export default get_clamped_value;
