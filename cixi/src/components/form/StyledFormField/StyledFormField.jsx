import React from 'react';
import PropTypes from 'prop-types';
import { GenericFormField } from 'generic-form';
import cn from 'classnames';

const StyledFormField = React.forwardRef(({
                                              className,
                                              icon,
                                              after,
                                              size,
                                              ...props
                                          }, ref) =>
    <GenericFormField
        ref={ref}
        className={cn('c-styled-form-field', className, size ? `${size}-input` : '')}
        {...props} />);


StyledFormField.propTypes = {
    className: PropTypes.string,
    icon: PropTypes.string,
    size: PropTypes.string,
    after: PropTypes.node
};

export default StyledFormField;
