import {connect}              from 'react-redux';
import { citiesSelector }                from '../../redux/content/cities/citiesSelectors';
import { compose, lifecycle, withProps } from 'recompose';
import Canvas                            from './Canvas';
import { roadsSelector }      from '../../redux/content/roads/roadsSelectors';
import combineSelectors       from '../../helpers/combineSelectors';
import { useLazyQuery }       from '@apollo/react-hooks';
import { gql }                from 'apollo-boost';
import { fetchCities }        from '../../redux/content/cities/citiesActions';
import { fetchRoads }         from '../../redux/content/roads/roadsActions';

const GET_SHORTEST_PATH = gql`
  query GetShortestPath($from: String!, $to: String!) {
    getShortestPath(from: $from, to: $to) {
        distance, 
        path
    }
  }
`;

const CanvasContainer = compose(
    connect(
        combineSelectors(
            citiesSelector,
            roadsSelector
        ),
        dispatch => ({
            fetchData: () => {
                dispatch(fetchCities());
                dispatch(fetchRoads());
            }
        })
    ),
    lifecycle({
        componentDidMount() {
            this.props.fetchData()
        }
    }),
    withProps(({
        cities,
        roads: _roads
               }) => {

        const roads = _roads.map(el => {
            const city_from = cities.find(city => city.name === el.from.name);
            const city_to = cities.find(city => city.name === el.to.name);

            const from_coordinates = {
                x: city_from.x,
                y: city_from.y,
            };
            const to_coordinates = {
                x: city_to.x,
                y: city_to.y,
            };

            return ({
                ...el,
                from: {
                    ...el.from,
                    ...from_coordinates
                },
                to: {
                    ...el.to,
                    ...to_coordinates
                }
            })
        });

        const [
            getShortestPath,
            { data: shortestPathData}
        ] = useLazyQuery(GET_SHORTEST_PATH);

        return ({
            getShortestPath,
            shortestPathData,
            cities,
            roads
        })
    })
)(Canvas);

export default CanvasContainer;
