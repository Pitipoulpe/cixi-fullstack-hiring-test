import React from 'react';
import { Stage, Layer, Text, Circle, Group, Line} from 'react-konva';

const  CITY_DEFAULT_COLOR = "#1FB4FF";
const  EDGE_DEFAULT_COLOR = "#dedede";
const  SHORTEST_PATH_COLOR     = "#BC69F0";
const  CITY_VISITED_COLOR     = "#555";
const  STARTING_CITY_COLOR = "#28ED56";
const  ENDING_CITY_COLOR   = "#FF3D44";

class Canvas extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            from: null,
            to: null,
            visitedCities: [],
            visitedRoads: [],
            shortestDistance: null
        };
    }

    componentDidUpdate( prevProps) {
        if (!prevProps.shortestPathData && this.props.shortestPathData) {
            const shortestPath = this.props.shortestPathData.getShortestPath;
            this.renderShortestPath(shortestPath);
        }
    }

    renderShortestPath = shortestPath => {
        const visitedCities = shortestPath.path;
        const visitedRoads = [];

        for (let index = 0; index <= visitedCities.length; index++) {
            const currentCity = shortestPath.path[index];
            const nextCity = shortestPath.path[index + 1];
            const findRoad = this.props.roads.find(road => road.from.name === currentCity && road.to.name === nextCity);
            if (findRoad)
                visitedRoads.push(findRoad.id);
        }

        this.setState({
            visitedCities,
            visitedRoads,
            shortestDistance: shortestPath.distance
        })

    };

    onTouchStart = name => {
        const {
            from,
            to
        } = this.state;
        if (!from) {
            this.setState({
                from: name,
                to: null,
                visitedCities: [],
                visitedRoads: [],
                shortestDistance: null
            })
        } else if (from && !to) {
            if (from === name) {
                this.setState({
                    from: null,
                    to: null,
                    visitedCities: [],
                    visitedRoads: [],
                    shortestDistance: null
                })
            } else {
                this.setState({
                    to: name
                }, () => this.calculateShortestPath())
            }

        } else {
            this.setState({
                from: null,
                to: null,
                visitedCities: [],
                visitedRoads: [],
                shortestDistance: null
            })
        }
    };

    calculateShortestPath = () => {
        this.props.getShortestPath({
            variables: {
                from: this.state.from,
                to: this.state.to
            }
        })
    };

    render() {
        const {
            cities,
            roads
        } = this.props;

        const {
            from,
            to,
            visitedCities,
            visitedRoads,
            shortestDistance
        } = this.state;

        return <div className="canvas">
            <h2>
                {visitedCities.length > 0 ? `The shortest path goes through ${visitedCities.join(', ')} and is ${shortestDistance}km long.` :
                    'Select cities to calculate the shortest path'
                }
            </h2>
            <Stage width={window.innerWidth} height={500}>

                <Layer>
                    {roads.map(({id, from, to}, i) => {
                        const color = visitedRoads.indexOf(id) > -1 ? SHORTEST_PATH_COLOR : EDGE_DEFAULT_COLOR;
                        const zIndex = visitedRoads.indexOf(id) > -1 ? 3 : 1;
                        const opacity = visitedRoads.indexOf(id) > -1 ? 1 : .2;

                        return (
                            <Group key={i}
                                   zIndex={zIndex}
                                   height={40}
                                   width={100}
                            >
                                <Line
                                    points={[
                                        from.x,
                                        from.y,
                                        to.x,
                                        to.y
                                    ]}
                                    opacity={opacity}
                                    zIndex={zIndex}
                                    stroke={color}
                                    strokeWidth={5}
                                    lineCap="round"
                                    lineJoin="round"
                                    shadowColor="black"
                                    shadowBlur={10}
                                    shadowOpacity={0.6}
                                />

                            </Group>

                        )
                    })}
                    {cities.map(({name, x, y}, i) => {
                        const fill = name === from
                            ? STARTING_CITY_COLOR : name === to ? ENDING_CITY_COLOR : visitedCities.indexOf(name) > -1
                                ? SHORTEST_PATH_COLOR : visitedCities.length > 0 ? CITY_VISITED_COLOR : CITY_DEFAULT_COLOR;
                        const zIndex = visitedCities.indexOf(name) > -1 ? 4 : 2;

                        return (
                            <Group key={i} height={40} width={100} x={x} y={y} zindex={ zIndex} >
                                <Text
                                    text={name.toUpperCase()}
                                    align="center"
                                    fontSize={15}
                                    fill="#FFF"
                                    height={40}
                                    width={100}
                                    verticalAlign="middle"
                                />
                                <Circle
                                    radius={20}
                                    fill={fill}
                                    shadowColor="black"
                                    shadowBlur={10}
                                    shadowOpacity={0.6}
                                    onClick={() => this.onTouchStart(name)}
                                />

                            </Group>

                        )
                    })}
                </Layer>
            </Stage>
        </div>;
    }
}

export default Canvas;

