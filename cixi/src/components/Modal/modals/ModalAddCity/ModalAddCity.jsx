import React                                  from 'react';
import './ModalAddCity.scss'
import GenericForm, { GenericFormFieldTypes } from 'generic-form';
import StyledFormField                        from '../../../form/StyledFormField/StyledFormField';

const ModalAddCity = ({
    onSubmit,
    onClose
}) => <div className="modal-add-city">
    <header>
        <h2>Add a new city</h2>
        <button className="close" onClick={() => onClose()}>X</button>
    </header>
    <p className="description">
        Please enter a new city
    </p>
    <GenericForm id="form-add-city"
                 onSubmit={onSubmit}>
        <StyledFormField
            name="name"
            id="name-team"
            formId="form-add-city"
            type={GenericFormFieldTypes.TEXT}
            value={''}
        />

        <div className="buttons">
            <button className="close" type="button" onClick={() => onClose()}>Close</button>
            <button className="submit" type={GenericFormFieldTypes.SUBMIT}>Submit</button>
        </div>

    </GenericForm>
</div>;

export default ModalAddCity;
