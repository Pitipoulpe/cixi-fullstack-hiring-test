import {connect}              from 'react-redux';
import { closeModal }         from '../../../../redux/modals/modalsActions';
import ModalAddCity           from './ModalAddCity';
import { compose, withProps } from 'recompose';
import { createCity }         from '../../../../redux/content/cities/citiesActions';

const ModalAddCityContainer = compose(
    connect(
        null,
        dispatch => ({
            onClose: () => dispatch(closeModal()),
            createCity: (name, cb) => dispatch(createCity(name, cb))
        })
    ),
    withProps(({
                   createCity,
                   onClose
               }) => ({
        onSubmit: (e, data) => {
            e.preventDefault();
            console.log({e, data});
            createCity(data.name, onClose);
        }
    }))
)(ModalAddCity);

export default ModalAddCityContainer;
