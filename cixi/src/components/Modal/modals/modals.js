import ModalAddCity from './ModalAddCity/ModalAddCity.container';
import ModalAddRoad from './ModalAddRoad/ModalAddRoad.container';

export const MODAL_ADD_CITY = 'add_city';
export const MODAL_ADD_ROAD = 'add_road';

export default {
    [MODAL_ADD_CITY]: ModalAddCity,
    [MODAL_ADD_ROAD]: ModalAddRoad
}
