import React                                  from 'react';
import GenericForm, { GenericFormFieldTypes } from 'generic-form';
import StyledFormField                        from '../../../form/StyledFormField/StyledFormField';

const ModalAddRoad = ({
    onSubmit,
    onClose,
    cities
}) => <div className="modal-add-road">
    <header>
        <h2>Add a new road</h2>
        <button className="close" onClick={() => onClose()}>X</button>
    </header>
    <p className="description">
        Please enter a new road between two cities
    </p>
    <GenericForm id="form-add-road"
                 onSubmit={onSubmit}>
        <div className="select-container">
            <StyledFormField
                name="from"
                id="from"
                formId="form-add-road"
                type={GenericFormFieldTypes.SELECT}
                options={cities.map(({ name }) => ({
                    label: name.toUpperCase(),
                    value: name
                }))}
            />
            <StyledFormField
                name="to"
                id="to"
                formId="form-add-road"
                type={GenericFormFieldTypes.SELECT}
                options={cities.reverse().map(({ name }) => ({
                    label: name.toUpperCase(),
                    value: name
                }))}
            />
        </div>
        <StyledFormField
            name="distance"
            id="distance"
            formId="form-add-road"
            type={GenericFormFieldTypes.NUMBER}
            value={10}
        />

        <div className="buttons">
            <button className="close" type="button" onClick={() => onClose()}>Close</button>
            <button className="submit" type={GenericFormFieldTypes.SUBMIT}>Submit</button>
        </div>

    </GenericForm>
</div>;

export default ModalAddRoad;
