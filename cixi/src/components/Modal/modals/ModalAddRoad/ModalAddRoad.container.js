import {connect}              from 'react-redux';
import { closeModal }         from '../../../../redux/modals/modalsActions';
import ModalAddRoad           from './ModalAddRoad';
import { compose, withProps } from 'recompose';
import { citiesSelector }     from '../../../../redux/content/cities/citiesSelectors';
import { createRoad }         from '../../../../redux/content/roads/roadsActions';

const ModalAddRoadContainer = compose(
    connect(
        citiesSelector,
        dispatch => ({
            onClose: () => dispatch(closeModal()),
            createRoad: (from, to, distance, cb) => dispatch(createRoad(from, to, distance, cb))
        })
    ),
    withProps(({
                   createRoad,
                   onClose
               }) => ({
        onSubmit: (e, data) => {
            e.preventDefault();

            createRoad(data.from, data.to, data.distance, onClose);
        }
    }))
)(ModalAddRoad);

export default ModalAddRoadContainer;
