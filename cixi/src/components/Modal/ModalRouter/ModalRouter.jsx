import React from 'react';
import modals from '../modals/modals';

const ModalRouter = ({
    currentModal,
    closeModal
}) => {
    if (!currentModal || !modals[currentModal]) return null;

    const Children = modals[currentModal];

    return <div className="c-modal-container">
        <div className="overlay" onClick={() => closeModal()}/>
        <div className="c-modal-content">
            <Children />
        </div>
    </div>
};

export default ModalRouter
