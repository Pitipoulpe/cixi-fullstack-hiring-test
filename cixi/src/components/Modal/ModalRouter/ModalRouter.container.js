import {compose}                from 'recompose';
import {connect}                from 'react-redux';
import combineSelectors         from '../../../helpers/combineSelectors';
import ModalRouter              from './ModalRouter';
import { closeModal }           from '../../../redux/modals/modalsActions';
import { currentModalSelector } from '../../../redux/modals/modalsSelectors';

const ModalRouterContainer = compose(
    connect(
        combineSelectors(
            currentModalSelector
        ),
        dispatch => ({
            closeModal: () => dispatch(closeModal())
        })
    )
)(ModalRouter);

export default ModalRouterContainer;


