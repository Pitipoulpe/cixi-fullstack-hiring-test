import React              from 'react';
import {Provider}         from 'react-redux';
import { ApolloProvider } from '@apollo/react-hooks';
import store              from '../../redux/store';
import logo               from '../../assets/logo.svg';
import './App.scss';
import ListCities         from '../ListCities/ListCities.container';
import ModalRouter        from '../Modal/ModalRouter/ModalRouter.container';
import apolloClient       from '../../redux/apolloClient';
import Canvas             from '../Canvas/Canvas.container';
import ListRoads          from '../ListRoads/ListRoads.container';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h2>Cixi - Hiring test</h2>
      </header>
        <main>
            <ApolloProvider client={apolloClient}>
                <Provider store={store}>
                    <div className="btn-container">
                        <ListCities />
                        <ListRoads />
                    </div>
                    <Canvas />
                    <ModalRouter />
                </Provider>
            </ApolloProvider>
        </main>
    </div>
  );
}

export default App;
