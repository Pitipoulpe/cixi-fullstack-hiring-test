import React from 'react';

const ListCities = ({
    openModalAddCity
}) => <button className="btn" onClick={() => openModalAddCity()}>add a city</button>;

export default ListCities;
