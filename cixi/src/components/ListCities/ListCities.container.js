import { compose, lifecycle } from 'recompose';
import {connect}                         from 'react-redux';
import ListCities             from './ListCities';
import { citiesSelector }     from '../../redux/content/cities/citiesSelectors';
import { fetchCities }        from '../../redux/content/cities/citiesActions';
import { openModal }          from '../../redux/modals/modalsActions';
import { MODAL_ADD_CITY }     from '../Modal/modals/modals';

const ListCitiesContainer = compose(
    connect(
        citiesSelector,
        dispatch => ({
            fetchCities: () => dispatch(fetchCities()),
            openModalAddCity: () => dispatch(openModal(MODAL_ADD_CITY))
        })
    ),
    lifecycle({
        componentDidMount() {
            this.props.fetchCities();
        }
    })
)(ListCities);

export default ListCitiesContainer;
