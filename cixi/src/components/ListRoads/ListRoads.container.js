import { compose, lifecycle } from 'recompose';
import {connect}              from 'react-redux';
import ListRoads              from './ListRoads';
import { openModal }          from '../../redux/modals/modalsActions';
import { MODAL_ADD_ROAD }     from '../Modal/modals/modals';
import { citiesSelector }     from '../../redux/content/cities/citiesSelectors';
import { fetchCities }        from '../../redux/content/cities/citiesActions';

const ListRoadsContainer = compose(
    connect(
        citiesSelector,
        dispatch => ({
            fetchCities: () => dispatch(fetchCities()),
            openModalAddRoad: () => dispatch(openModal(MODAL_ADD_ROAD))
        })
    ),
    lifecycle({
        componentDidMount() {
            if (!this.props.cities.length)
                this.props.fetchCities();
        }
    })
)(ListRoads);

export default ListRoadsContainer;
