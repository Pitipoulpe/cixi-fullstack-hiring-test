# Cixi - Hiring test 🤖

Full Stack developer hiring test for Cixi

## Technologies 🤖

This project is built with [node.js](https://nodejs.org/en/) and [TypeScript](https://www.typescriptlang.org/) to provide robust and evolutive dev environment.

To run, our server use [Express](https://expressjs.com/) and provide a GraphQL api thank's to [Apollo](https://www.apollographql.com/)🚀.

The Database engine is [Postgres](https://www.postgresql.org/)🐘. The node library chosen to manage the communication with the engine is [pg ( node-postgres )](https://node-postgres.com/) and it's use [typeorm](https://typeorm.io/#/) as ORM. 
This client offer an interesting implementation with some nice features but also impressive performances 📈.

The frontOffice is developped with [React](https://fr.reactjs.org/) and [Redux](https://redux.js.org/) ❤️ and use [Apollo Client](https://www.apollographql.com/docs/react/)🚀 library to comunicate with the API.
It's use [Konvajs](https://konvajs.org/) 🎨 for the creation and the canvas's interactions. 

## How to run it locally ? 👨‍💻

> ⚠️ Make sure you have [node.js](https://nodejs.org/en/), [yarn](https://yarnpkg.com/) and [Docker](https://docs.docker.com/install/) installed on your computer.


To launch the app for the first time you need to open a terminal and run the command

```
cd ./cixi && yarn && ../api && yarn && ../
```
This command will install all the dependences for the Api and th front project.

Now you can run the entire App with the command 

```
yarn start
```
> ⚠️ Make sure you are at the root of the project to run this command

It will start your postgres container and your app.
You can then access to your app on [http://localhost:3000/]
And your Api on [http://localhost:4000/playground]

## Generate minimum data ? ⚙️
You can generate the data of the subject on [http://localhost:4000/playground]
enter the query 

```
mutation {
	generateData
}
```

![query in graphQL](https://gitlab.com/Pitipoulpe/cixi-fullstack-hiring-test/-/raw/master/githubImages/graphql-mut.png) 

You can then access to your [App](http://localhost:3000/)

And ... Voilà ! 

![Result](https://gitlab.com/Pitipoulpe/cixi-fullstack-hiring-test/-/raw/master/githubImages/result.png) 

## Thanks ✨✨
Thanks to [Julien Pons](https://gitlab.com/julien.pons.gillot03) for all his advice and for all the discussions we had.
