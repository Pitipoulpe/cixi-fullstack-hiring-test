
/*create schema api;

CREATE TYPE unit_type AS ENUM ('minute', 'kilometer');

create table api.cities (
  id serial primary key,
  name text not null unique,
  created_at TIMESTAMPTZ not null default NOW()
);

insert into api.cities (name) values
  ('Annecy'),
  ('Seynod'),
  ('Metz-Tessy'),
  ('Epagny'),
  ('Sillingy');

create table api.roads (
  id serial primary key,
  city_a_id INTEGER REFERENCES api.cities(id),
  city_b_id  INTEGER REFERENCES api.cities(id),
  distance integer,
  unit unit_type not null default 'minute'
);

insert into api.roads (city_a_id, city_b_id, distance) values
  (1, 2, 9),
  (2, 3, 4),
  (1, 3, 12),
  (3, 4, 3),
  (4, 5, 1),
  (2, 5, 11),
  (1, 5, 19);

create role authenticator noinherit login password 'mysecretpassword';
grant app_user to authenticator;

create role api_user nologin;
grant api_user to authenticator;

grant usage on schema api to api_user;
grant all on api.cities to api_user;
grant all on api.roads to api_user;
grant usage, select on sequence api.cities_id_seq to api_user;
grant usage, select on sequence api.roads_id_seq to api_user;*/


