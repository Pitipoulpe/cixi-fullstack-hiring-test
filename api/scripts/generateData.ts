import {City} from "../src/entity/City";
import {Road} from "../src/entity/Road";

async function asyncForEach(array:any[], callback:Function) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}

const generateData = async () => {
    const cities = [
        'Annecy',
        'Metz',
        'Seynod',
        'Epagny',
        'Silligny'
    ];
    const roads = [
        {
            from: 'Annecy', to: 'Metz', distance: 12
        },
        {
            from: 'Annecy', to: 'Silligny', distance: 19
        },
        {
            from: 'Annecy', to: 'Seynod', distance: 9
        },
        {
            from: 'Metz', to: 'Annecy', distance: 12
        },
        {
            from: 'Metz', to: 'Seynod', distance: 4
        },
        {
            from: 'Metz', to: 'Epagny', distance: 3
        },
        {
            from: 'Seynod', to: 'Annecy', distance: 9
        },
        {
            from: 'Seynod', to: 'Silligny', distance: 11
        },
        {
            from: 'Seynod', to: 'Metz', distance: 4
        },
        {
            from: 'Epagny', to: 'Metz', distance: 3
        },
        {
            from: 'Epagny', to: 'Silligny', distance: 1
        },
        {
            from: 'Silligny', to: 'Epagny', distance: 1
        },
        {
            from: 'Silligny', to: 'Annecy', distance: 19
        }
    ];
    await asyncForEach(cities, async (name:string) => {
        const alreadyExist = await City.findOne({where: {name}});
        if (alreadyExist)
            return;

        await City.create({
            name
        }).save();
        return true;
    });

    await asyncForEach(roads, async (road:Road) => {
        const alreadyExist = await Road.findOne({where: {
            from: {name: road.from},
            to: {name: road.to},
            distance: road.distance
            }, relations: ['from', 'to']});
        if (alreadyExist)
            return;

        const cityFrom = await City.findOne({where: {name:road.from}});
        const cityTo = await City.findOne({where: {name:road.to}});
        if (!cityFrom || !cityTo)
            return;
        const nRoad = await Road.create({
            distance: road.distance
        });
        nRoad.from = cityFrom;
        nRoad.to = cityTo;

        await nRoad.save();
        return true;
    });

    return true;
};

export default generateData;
