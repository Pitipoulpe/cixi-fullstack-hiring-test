import { gql } from "apollo-server-express";

export const typeDefs = gql`
    type City {
        id: ID!
        name: String!
    }
    
    type Road {
        id: ID!
        from: City
        to: City
        distance: Int!
    }
    
    type ShortestPath {
        distance: Int
        path: [String]
    }

    type Query {
        getRoads: [Road]!
        getCities: [City]!
        getCity(cityName: String!): City!
        getShortestPath(from: String!, to: String!): ShortestPath!
    },

    type Mutation {
        addCity(name: String!): City!
        addRoad(from: String!, to: String!, distance: Int!): Road!
        generateData: Boolean

    }
`;
