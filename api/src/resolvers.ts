import {IResolvers} from "graphql-tools";
import {City} from "./entity/City";
import {Road} from "./entity/Road";
import findShortestPath from "./helpers/findShortesPath";
import generateData from "../scripts/generateData";

interface IRoadType {
    from: string;
    to: string;
    distance: number;
}

export const resolvers: IResolvers = {
    Query: {
        getCities: async (_, __) => {
            return await City.find();
        },
        getCity: async (_, {cityName} ) => {

            const city = await City.findOne({where: {name: cityName}});
            if (!city)
                throw new Error('city not found ...');

            return ({
                id: city.id,
                name: city.name
            });
        },
        getRoads: async () => {
            return await Road.find({relations: ["from", "to"]});
        },
        getShortestPath: async (_, {from, to}) => {
            const roads = await Road.find({relations: ["from", "to"]});
            if (!roads)
                throw new Error('Error get roads ...');

            const graph = (roads as any).map((el:Road) => ({
                ...el,
                from: el.from.name,
                to: el.to.name
            })).reduce( (
                acc: object,
                {from, to, distance}: IRoadType
                ):object => {
                const current: object = (acc as any)[from] || {};
                (acc as any)[from] = {
                    ...current,
                    [to]: distance
                };
                return acc
            }, ({} as object));

            if (!graph[from] && !graph[to])
                throw new Error(`Cities ${from} and ${to} don't exist`);
            if (!graph[from])
                throw new Error(`City ${from} doesn't exist`);
            if (!graph[to])
                throw new Error(`City ${to} doesn't exist`);


            return findShortestPath(graph, from, to);
        }
    },
    Mutation: {
        addCity: async (_, {name}) => {
            const alreadyExist = await City.findOne({where: {name}});
            if (alreadyExist)
                throw new Error('City already exists ...');
            await City.create({
                name
            }).save();

            return await City.findOne({where: {name}});
        },
        addRoad: async (_, {from, to, distance}) => {
            const alreadyExist = await Road.findOne({
                where: {
                    from: {
                        name:from
                    },
                    to: {
                        name:to
                    },
                    distance
            }, relations:["from", "to"]});
            if (alreadyExist)
                throw new Error('Road already exists ...');
            const getCityFrom = await City.findOne({where:{name:from}});
            const getCityTo = await City.findOne({where:{name:to}});

            if (!getCityFrom || !getCityTo)
                throw new Error('Cities doesnt exist');
            const newRoad = Road.create({
                distance
            });
            newRoad.from = getCityFrom;
            newRoad.to = getCityTo;

            console.log({newRoad});

            const save = await newRoad.save();
            console.log({save});
            return save;
        },
        generateData: async () => await generateData()
    }
};
