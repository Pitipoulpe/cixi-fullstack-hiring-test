import { Entity, Column, BaseEntity, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import {City} from "./City";

@Entity("roads")
export class Road extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => City, city => city.roadsFrom)
    from: City;

    @ManyToOne(() => City, city => city.roadsTo)
    to: City;

    @Column('int')
    distance: number;

}
