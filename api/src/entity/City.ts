import {Entity, Column, BaseEntity, PrimaryGeneratedColumn, OneToMany} from "typeorm";
import {Road} from "./Road";

@Entity("cities")
export class City extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar", { length: 255, unique: true })
    name: string;

    @OneToMany(() => Road, road => road.from)
    roadsFrom: Road[];

    @OneToMany(() => Road, road => road.to)
    roadsTo: Road[];
}
