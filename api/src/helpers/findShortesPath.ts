/*const graph = {
    Annecy: { Metz: 12, Silligny: 19, Seynod: 9 },
    Metz: { Annecy: 12, Seynod: 4, Epagny: 3 },
    Seynod: { Annecy: 9, Silligny: 11, Metz:4 },
    Epagny: { Metz: 3, Silligny: 1 },
    Silligny: { Epagny: 1, Annecy:19 }
};*/

const shortestDistance = (distances: object , visited:any[]) => {
    // create a default value for shortest
    let shortest = null;

    // for each city in the distances object
    for (let city in distances) {
        if (distances.hasOwnProperty(city)) {
            // if no city has been assigned to shortest yet
            // or if the current city's distance is smaller than the current shortest
            let currentIsShortest =
                shortest === null || (distances as any)[city] < (distances as any)[shortest];

            // and if the current city is in the unvisited set
            if (currentIsShortest && !visited.includes(city)) {
                // update shortest to be the current city
                shortest = city;
            }
        }
    }
    return shortest;
};

const findShortestPath = (
    graph: object,
    startCity: string,
    endCity: string
) => {

    // track distances from the start city using a hash object
    let distances = {};
    (distances as any)[endCity] = "Infinity";
    distances = { ...distances, ...(graph as any)[startCity]};
    // track paths using a hash object
    let parents = { endCity: null };
    for (let child in (graph as any)[startCity]) {
        if((graph as any)[startCity].hasOwnProperty(child)) {
            (parents as any)[child] = startCity;
        }
    }
    // collect visited cities
    const visited:any[] = [];
    // find the nearest city
    let city = shortestDistance(distances, visited);

    // for that city:
    while (city) {
        // find its distance from the start city & its child cities
        const distanceFromStartCity = (distances as any)[city];
        const children = (graph as any)[city];

        // for each of those child cities:
        for (let child in children) {
            if (children.hasOwnProperty(child)) {
                // make sure each child city is not the start city
                if (String(child) !== String(startCity)) {
                    // save the distance from the start city to the child city
                    const newDistance = distanceFromStartCity + children[child];
                    // if there's no recorded distance from the start city to the child city in the distances object
                    // or if the recorded distance is shorter than the previously stored distance from the start city to the child city
                    if (!(distances as any)[child] || (distances as any)[child] > newDistance) {
                        // save the distance to the object
                        (distances as any)[child] = newDistance;
                        // record the path
                        (parents as any)[child] = city;
                    }
                }
            }
        }
        // move the current city to the visited set
        visited.push(city);
        // move to the nearest neighbor city
        city = shortestDistance(distances, visited);
    }

    // using the stored paths from start city to end city
    // record the shortest path
    const shortestPath = [endCity];
    let parent = (parents as any)[endCity];
    while (parent) {
        shortestPath.push(parent);
        parent = (parents as any)[parent];
    }
    shortestPath.reverse();

    //this is the shortest path
    return {
        distance: (distances as any)[endCity],
        path: shortestPath,
    };
};

export default findShortestPath;
