import * as express from 'express';
import { ApolloServer } from "apollo-server-express";
import { typeDefs } from "./typeDefs";
import { resolvers } from "./resolvers";
import { createConnection } from "typeorm";
import expressPlayground from 'graphql-playground-middleware-express';

const startServer = async () => {
    const server = new ApolloServer({
        typeDefs,
        resolvers,
        introspection: true,
        playground: true
    });

    const tryReconnect = async () => {
        try {
            console.log(('Try connexion ....'));
            await createConnection();
            console.log(('Connexion DB success !!'));
        } catch (e) {
            console.log(e);
            await new Promise(resolve => setTimeout(resolve, 1000));
            await tryReconnect();
        }
    };

    await tryReconnect();

    const app = express();

    const path = process.env.API_PATH;
    const playground_path = process.env.API_PLAYGROUND || '/playground';

    const port = parseInt(process.env.API_PORT || '4000');

    server.applyMiddleware({ app, path });

    app.get(playground_path, expressPlayground({ endpoint: path }));

    app.listen({ port }, () => {
        console.log(`🚀  Server ready at ${server.graphqlPath} `);
    }

    );
};

startServer();
